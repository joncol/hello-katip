{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Main
  ( main,
  )
where

-------------------------------------------------------------------------------
import qualified Control.Applicative as A
import Control.Exception
import Control.Monad.Base
import Control.Monad.Reader
import Control.Monad.Trans.Control
import Data.Aeson ((.=))
import qualified Data.Aeson as A
import qualified Data.Map as Map
import qualified Data.HashMap as HM
import qualified Data.HashMap.Strict as HMS
import qualified Data.Monoid as M
-------------------------------------------------------------------------------

import Data.Text.Lazy (toStrict)
import Data.Text.Lazy.Builder (fromText)
import qualified Data.Text.Lazy.Builder as B
import Data.Text.Lazy.Encoding (decodeUtf8)
import Katip
import qualified Katip.Core as KC
import Katip.Scribes.Handle (colorBySeverity)
import Lens.Micro
import System.IO (stdout)
import Data.Function ((&))
import Data.Hashable (Hashable)
import Data.List (foldl')

-------------------------------------------------------------------------------

main :: IO ()
main = do
  handleScribe <-
    mkHandleScribeWithFormatter
      myJsonFormat
      (ColorLog False)
      stdout
      (permitItem DebugS)
      V2
  let mkLogEnv = registerScribe "stdout" handleScribe defaultScribeSettings =<< initLogEnv "MyApp" "production"
  bracket mkLogEnv closeScribes $ \le -> do
    let s = MyState M.mempty mempty le
    runStack s $ do
      $(logTM) InfoS "Started"
      katipAddNamespace "confrabulation" $
        katipAddContext (ConfrabLogCTX 42) $ do
          $(logTM) DebugS "Confrabulating widgets, with extra namespace and context"
          logLocM DebugS "Another way of logging, without the need of TemplateHaskell"
          confrabulateWidgets
      $(logTM) InfoS "Namespace and context are back to normal"
      katipNoLogging $
        $(logTM) DebugS "You'll never see this log message!"

-------------------------------------------------------------------------------
myJsonFormat :: LogItem a => ItemFormatter a
myJsonFormat withColor verb i =
  fromText $
    colorBySeverity withColor (_itemSeverity i) $
      toStrict $ decodeUtf8 $ A.encode $ myLogItemConverter $ KC.itemJson verb i

myLogItemConverter :: A.Value -> A.Value
myLogItemConverter x = case x of
  (A.Object m) -> A.Object $ foldl' (\acc (from, to) -> renameKey acc from to) m
    [("sev", "level"),
     ("app", "service"),
     ("at", "timestamp"),
     ("msg", "message")]
  _ -> x

renameKey :: (Hashable k, Ord k) => HMS.HashMap k a -> k -> k -> HMS.HashMap k a
renameKey hm oldKey newKey =
  case Map.updateLookupWithKey (\_ _ -> Nothing) oldKey map of
     (Nothing, _) -> hm
     (Just val, map') -> HMS.insert newKey val (HMS.fromList . Map.toList $ map')
  where
    map = Map.fromList . HMS.toList $ hm

-------------------------------------------------------------------------------
newtype ConfrabLogCTX = ConfrabLogCTX Int

instance A.ToJSON ConfrabLogCTX where
  toJSON (ConfrabLogCTX factor) = A.object ["confrab_factor" .= factor]

instance ToObject ConfrabLogCTX

instance LogItem ConfrabLogCTX where
  payloadKeys _verb _a = AllKeys

-------------------------------------------------------------------------------
confrabulateWidgets :: (Monad m) => m ()
confrabulateWidgets = return ()

-------------------------------------------------------------------------------
data MyState = MyState
  { msKNamespace :: Namespace,
    msKContext :: LogContexts,
    msLogEnv :: LogEnv
  }

-------------------------------------------------------------------------------
newtype MyStack m a = MyStack
  { unStack :: ReaderT MyState m a
  }
  deriving (MonadReader MyState, Functor, A.Applicative, Monad, MonadIO, MonadTrans)

-- MonadBase, MonadTransControl, and MonadBaseControl aren't strictly
-- needed for this example, but they are commonly required and
-- MonadTransControl/MonadBaseControl are a pain to implement, so I've
-- included them. Note that KatipT and KatipContextT already do this work for you.
instance MonadBase b m => MonadBase b (MyStack m) where
  liftBase = liftBaseDefault

instance MonadTransControl MyStack where
  type StT MyStack a = StT (ReaderT Int) a
  liftWith = defaultLiftWith MyStack unStack
  restoreT = defaultRestoreT MyStack

instance MonadBaseControl b m => MonadBaseControl b (MyStack m) where
  type StM (MyStack m) a = ComposeSt MyStack m a
  liftBaseWith = defaultLiftBaseWith
  restoreM = defaultRestoreM

instance (MonadIO m) => Katip (MyStack m) where
  getLogEnv = asks msLogEnv
  localLogEnv f (MyStack m) = MyStack (local (\s -> s {msLogEnv = f (msLogEnv s)}) m)

instance (MonadIO m) => KatipContext (MyStack m) where
  getKatipContext = asks msKContext
  localKatipContext f (MyStack m) = MyStack (local (\s -> s {msKContext = f (msKContext s)}) m)
  getKatipNamespace = asks msKNamespace
  localKatipNamespace f (MyStack m) = MyStack (local (\s -> s {msKNamespace = f (msKNamespace s)}) m)

-------------------------------------------------------------------------------
runStack :: MyState -> MyStack m a -> m a
runStack s f = runReaderT (unStack f) s

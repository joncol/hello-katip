{ sources ? import ./nix/sources.nix,
  pkgs ? (import sources.nixpkgs {})
}:

let
  t = pkgs.lib.trivial;
  hl = pkgs.haskell.lib;

  pkg = pkgs.haskellPackages.developPackage {
    root = ./.;

    modifier = (t.flip t.pipe)
      [hl.dontHaddock
       hl.enableStaticLibraries
       hl.justStaticExecutables
       hl.disableLibraryProfiling
       hl.disableExecutableProfiling];
  };

in { inherit pkg; }
